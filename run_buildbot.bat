call "C:/wenv/x64/activate.bat"
@echo on

:: Create Directory for buildbot installation
mkdir C:\Workspace
cd C:\Workspace

:: Create virtual venv
python -m venv venv

:: Activate venv
call ".\venv\Scripts\activate"
@echo on

:: Install buildbot-worker in virtual env
python -m pip install pypiwin32 buildbot-worker

:: Dectivate venv
call ".\venv\Scripts\deactivate"
@echo on

:::::::::::::::::::::::::::::::::::::::::::

:: Note: PATH must be set *before* activating a venv.
:: Else, a nested venv (python test_venv does it) will restore older PATH.

:: Add missing debug runtime DLL to path (all arch)
:: ucrtbased.dll
set PATH=C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\arm64\ucrt\;%PATH%
set PATH=C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x86\ucrt\;%PATH%
set PATH=C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x64\ucrt\;%PATH%
:: vcruntime140d.dll (all arch)
set PATH=C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Redist\MSVC\14.31.31103\onecore\debug_nonredist\arm64\Microsoft.VC143.DebugCRT;%PATH%
set PATH=C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Redist\MSVC\14.31.31103\onecore\debug_nonredist\x86\Microsoft.VC143.DebugCRT;%PATH%
set PATH=C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Redist\MSVC\14.31.31103\onecore\debug_nonredist\x64\Microsoft.VC143.DebugCRT;%PATH%

:: activate arm64 compilation env
call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64_arm64

::::::::::::::::::::::::::::::::::::::::::::

:: Activate venv
call ".\venv\Scripts\activate"
@echo on

:: delete older config
rm buildarea/buildbot.tac
:: create worker (will update if it exists)
buildbot-worker create-worker buildarea buildbot-api.python.org:9020 LOGIN PASSWORD

:: add our email for this worker
echo Linaro Windows on Arm Support ^<woa-support@op-lists.linaro.org^> > buildarea\info\admin

:: add config for this worker
echo Azure ARM server; 4 cores; RAM 16GB; Windows 11 Pro (ARM64) > buildarea\info\host

:: Start buildbot-worker
@ echo Starting buildbot-workder
buildbot-worker start buildarea
